require 'fox16'
include Fox

application = FXApp.new
main = FXMainWindow.new(application, "Task 1")

now = Time.new
button = FXButton.new(main, "#{now.day}/#{now.month}/#{now.year}")

application.create
main.show(PLACEMENT_SCREEN)
application.run
